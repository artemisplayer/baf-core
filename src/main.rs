// --- BAF CORE ---
// Nathan Gasc - 18/12/2023
// Avancement: le programme ne permet que de construire les fichier .baf 
// et de les supprimer, les fichiers sont traités différement des dossiers.


use std::{io, os::unix::fs::MetadataExt};
use sha2::{Sha256, Digest};
use std::fs::{File, read_dir, DirEntry, remove_file};
use mime_guess;
use std::io::{Write, BufReader,BufRead};

struct FileEntry{
    path: String,
    key: String,
    typ: String,
    ctime: i64,
    mtime: i64,
    size: u64,
}

impl ToString for FileEntry{
    fn to_string(&self) -> String {
        format!("file {} {} {} {} {} {}\n", self.path, self.key, self.typ, self.ctime, self.mtime, self.size)
    }
}

struct DirectoryEntry{
    path: String,
    key: String,
    elements: Vec<[String; 3]>, // (directory/file, key, path)
}

impl ToString for DirectoryEntry{
    fn to_string(&self) -> String {
        let str_elements: String = self.elements.iter().map(|e: &[String; 3]| format!("({} {} {})", e[0], e[1], e[2])).collect();
        format!("dir {} {} {}\n", self.path, self.key, str_elements)
    }
}

fn get_file_hash(file_path: &str) -> String {
    let mut file = File::open(file_path).expect("Failed to open file");
    let mut hasher = Sha256::new();
    io::copy(&mut file, &mut hasher).expect("Failed to read file");
    format!("{:x}", hasher.finalize())
}

fn get_file_mime(file_path: &str) -> String {
    let mime = mime_guess::from_path(file_path).first_or_octet_stream();
    mime.as_ref().to_string()
}

fn get_file_size_ctime_mtime(file_path: &str) -> (u64, i64, i64) {
    let file: File = File::open(file_path).expect("Failed to open file");
    let metadata: std::fs::Metadata = file.metadata().expect("Failed to read file");
    (metadata.len(), metadata.ctime(), metadata.mtime())
}

fn build_file_entry(file_path: &str) -> FileEntry {
    let (size, ctime, mtime) = get_file_size_ctime_mtime(file_path);
    FileEntry{
        path: file_path.to_string(),
        key: get_file_hash(file_path),
        typ: get_file_mime(file_path),
        ctime: ctime,
        mtime: mtime,
        size: size,
    }
}

fn build_directory_entry(path: &str) -> DirectoryEntry{
    // We first need to create the .baf file inside the directory
    
    create_baf_file(&path);
    let baf_ok: File = File::open(path.to_owned() + "/.baf").unwrap();

    //first read File to lines:
    let mut buf: Vec<String> = Vec::new();
    for line in BufReader::new(baf_ok).lines(){
        buf.push(line.unwrap());
    }

    // we build our elements vector
    // each element is (directory/file, key, path)
    let mut elements : Vec<[String; 3]> = Vec::new();

    let mut keys: Vec<String> = Vec::new();
    for line in buf{
        let ligne: Vec<&str> = line.split_whitespace().collect::<Vec<&str>>();
        let k: String = ligne[2].to_string();
        let typ: &str = if ligne[0] == "file" {"file"} else {"dir"};
        let p: &str = ligne[1];
        elements.push([typ.to_string(), k.clone(), p.to_string()]);
        keys.push(k);
    }

    //the key is the sha256 of all ordered keys
    keys.sort();
    let mut hasher = Sha256::new();
    hasher.update(keys.join(" ").as_bytes());
    let key = hasher.finalize();

    DirectoryEntry{
        path: path.to_string(),
        key: format!("{:x}", key),
        elements: elements,
    }
}


fn create_baf_file(path: &str) {
    // analyze the given path, create a FileEntry for each file, and store them into a '.baf' file
    let mut files: Vec<String> = Vec::new();
    let mut directories: Vec<String> = Vec::new();
    for entry in read_dir(path).expect("Failed to read directory") {
        let entry: DirEntry = entry.expect("Failed to read entry");
        let path: std::path::PathBuf = entry.path();
        if path.is_file() {
            files.push(path.to_str().unwrap().to_string());
        } else {
            directories.push(path.to_str().unwrap().to_string());
        }
    }

    let mut baf: File = File::create(path.to_owned() + "/.baf").unwrap();

    for directory in directories{
        let dir_entry: DirectoryEntry = build_directory_entry(&directory);
        baf.write_all(dir_entry.to_string().as_bytes()).unwrap();
    }

    for fichier in files {
        let file_entry: FileEntry = build_file_entry(&fichier);
        
        baf.write_all(file_entry.to_string().as_bytes()).unwrap();
    }
}

fn remove_recursively_baf_files(path: &str){
    let mut files = read_dir(path).unwrap();
    while let Some(file) = files.next() {
        let file: DirEntry = file.unwrap();
        let name: String = file.file_name().into_string().unwrap();
        let path = file.path();
        if path.is_dir() {
            remove_recursively_baf_files(path.to_str().unwrap());
        } else {
            if name == "baf" {
                remove_file(path).unwrap();
            }
        }
    }
}

fn main() {
    // usage : /baf-core <path> [build/remove] 
    let path: String = std::env::args().nth(1).unwrap();
    let build: String = std::env::args().nth(2).unwrap();
    if build == "build" {
        create_baf_file(path.as_str());
    } else if build == "remove" {
        remove_recursively_baf_files(path.as_str());
    } else {
        println!("Usage : /baf-core <path> [build/remove]");
    }
}
